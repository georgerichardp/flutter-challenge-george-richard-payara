import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class CommonWidget {
  static Widget rowWidth([double width = 16]) {
    return SizedBox(
      width: width,
    );
  }

  static Widget rowHeight([double height = 16]) {
    return SizedBox(
      height: height,
    );
  }

  static void showDialog({required Widget builder}) {
    Get.dialog(
      builder,
      transitionCurve: Curves.slowMiddle,
      transitionDuration: const Duration(milliseconds: 200),
    );
    // showGeneralDialog(
    //   // barrierColor: Colors.black.withOpacity(0.5),
    //   transitionBuilder: (context, a1, a2, widget) {
    //     return Transform.scale(
    //       scale: a1.value,
    //       child: Opacity(
    //         opacity: a1.value,
    //         child: widget,
    //       ),
    //     );
    //   },
    //   transitionDuration: const Duration(milliseconds: 200),
    //   barrierDismissible: true,
    //   barrierLabel: '',
    //   context: context,
    //   pageBuilder: (BuildContext context, Animation<double> animation,
    //       Animation<double> secondaryAnimation) {
    //     return builder;
    //   },
    // );
  }

  static void showLoading() {
    dismissKeyboard();
    EasyLoading.show(
      indicator: Center(
        child: SizedBox(
          width: 85,
          height: 85,
          child: Card(
            elevation: 70,
            shadowColor: Colors.blueGrey.withOpacity(0.4),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100)),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: 35,
                    height: 35,
                    child: Image.asset("assets/icons/logo.png"),
                  ),
                ),
                const SizedBox(
                    width: 85, height: 85, child: CircularProgressIndicator())
              ],
            ),
          ),
        ),
        widthFactor: 0,
        heightFactor: 0,
      ),
    );
  }

  static void dismissLoading() {
    EasyLoading.dismiss();
  }

  static void dismissKeyboard() {
    if (FocusManager.instance.primaryFocus != null) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }

  static void showSnackBar(String message,
      {String? title, bool isSuccess = false, Duration? duration}) {
    Get.showSnackbar(GetSnackBar(
      message: message,
      title: title,
      duration: duration ?? Duration(milliseconds: isSuccess ? 1500 : 3000),
      backgroundColor: isSuccess ? Colors.lightGreen : Colors.red,
    ));
  }

  static Future<void> showInfo(String message, {bool isSuccess = false}) async {
    await Get.dialog(
      Center(
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: SizedBox(
            width: double.infinity,
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        border: Border.all(
                            color: isSuccess
                                ? Colors.lightGreen
                                : Colors.redAccent,
                            width: 2),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          isSuccess ? Icons.check : Icons.close,
                          color:
                              isSuccess ? Colors.lightGreen : Colors.redAccent,
                        ),
                      ),
                    ),
                    rowHeight(),
                    Text(
                      message,
                      style: const TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    rowHeight(),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: isSuccess ? null : Colors.redAccent,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 32, vertical: 8),
                      ),
                      onPressed: () {
                        Get.back();
                      },
                      child: Text("ok".tr),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  static Future<void> showAlert(String message,
      {bool isSuccess = false,
      Widget? icon,
      required VoidCallback pressedOk}) async {
    await Get.dialog(
      Center(
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: SizedBox(
            width: double.infinity,
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    icon ??
                        const Icon(
                          Icons.info_outline,
                          size: 35,
                          color: Colors.lightGreen,
                        ),
                    rowHeight(),
                    Text(
                      message,
                      style: const TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    rowHeight(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        OutlinedButton(
                          style: OutlinedButton.styleFrom(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 32, vertical: 8),
                          ),
                          onPressed: () {
                            Get.back();
                          },
                          child: Text("cancel".tr),
                        ),
                        rowWidth(),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 32, vertical: 8),
                          ),
                          onPressed: () {
                            Get.back();
                            pressedOk();
                          },
                          child: Text("ok".tr),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
