class SimpleModel<Datum> {
  String title;
  int type;
  Datum? data;

  SimpleModel({
    required this.title,
    this.type = 0,
    this.data,
  });
}
