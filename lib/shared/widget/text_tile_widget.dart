import 'package:flutter/material.dart';
import 'package:flutter_challenge_george/shared/common/common_widget.dart';
import 'package:get/get.dart';

class TextTileWidget extends StatelessWidget {
  final String title, subTitle;
  const TextTileWidget({
    Key? key,
    this.title = "",
    this.subTitle = "",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title,style: context.textTheme.headline6,),
        Text(subTitle),
        CommonWidget.rowHeight()
      ],
    );
  }
}
