import 'package:flutter/material.dart';
import 'package:flutter_challenge_george/shared/common/common_widget.dart';
import 'package:get/get.dart';

class TextFieldWidget extends StatelessWidget {
  final String labelText;
  final String? hintText;
  final TextInputType? keyboardType;
  final bool obscureText;
  final Widget? suffixIcon;
  final TextEditingController? controller;
  final String? Function(String? value)? validator;
  final  TextInputAction? textInputAction;
  const TextFieldWidget({
    Key? key,
    required this.labelText,
    this.hintText,
    this.keyboardType,
    this.suffixIcon,
    this.textInputAction,
    this.obscureText=false,
    this.controller,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CommonWidget.rowHeight(24),
        Text(labelText,style: context.textTheme.subtitle1,),
        TextFormField(
          controller: controller,
          decoration: InputDecoration(
            hintText: hintText,
            suffixIcon: suffixIcon,
            hintStyle: const TextStyle(fontStyle: FontStyle.italic)
          ),
          keyboardType: keyboardType,
          validator: validator,
          obscureText:obscureText ,
          textInputAction:textInputAction ,
        ),
      ],
    );
  }
}
