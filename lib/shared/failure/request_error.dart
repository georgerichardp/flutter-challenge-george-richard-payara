class RequestError extends Error {
  final String message;
  final String title;
  final int code;

  RequestError({
    required this.message,
    this.title = "",
    this.code = 0,
  });
}
