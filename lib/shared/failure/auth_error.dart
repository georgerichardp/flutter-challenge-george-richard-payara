class AuthError extends Error {
  final String message;
  final String title;
  final int code;

  AuthError({
    required this.message,
    this.title = "",
    this.code = 0,
  });
}
