class ConstantStorage{
  static const String userId="user_id";
  static const String initialApp="initial_app";
  static const String initialLogin="initial_login";
  static const String language="language";
  static const String listAuth="list_auth";
}