import 'package:flutter_challenge_george/modules/auth/auth_binding.dart';
import 'package:flutter_challenge_george/modules/auth/login_page.dart';
import 'package:flutter_challenge_george/modules/home/home_binding.dart';
import 'package:flutter_challenge_george/modules/home/home_page.dart';
import 'package:flutter_challenge_george/modules/splash/splash_binding.dart';
import 'package:flutter_challenge_george/modules/splash/splash_page.dart';
import 'package:get/get.dart';
import 'app_routes.dart';

class AppPages {
  static const initial = AppRoutes.splash;

  static final routes = [
    GetPage(
      name: AppRoutes.splash,
      page: () => const SplashPage(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: AppRoutes.auth,
      page: () => const LoginPage(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: AppRoutes.home,
      page: () => const HomePage(),
      binding: HomeBinding(),
    ),
  ];
}

Future<dynamic> goToHome()async {
  return Get.offAndToNamed(AppRoutes.home);
}
Future<dynamic> goToAuth() async {
  return Get.offAndToNamed(AppRoutes.auth);
}
Future<dynamic> goToInitial() async {
  return Get.offAndToNamed(AppRoutes.splash);
}
