class AppRoutes {
  static const splash = '/';
  static const auth = '/auth';
  static const login = '/login';
  static const register = '/register';
  static const home = '/home';
}
