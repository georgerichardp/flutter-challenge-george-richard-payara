import 'package:flutter_challenge_george/core/application/feature/auth/query/detail/user_detail_handler.dart';
import 'package:flutter_challenge_george/core/application/feature/auth/query/detail/user_detail_query.dart';
import 'package:flutter_challenge_george/core/application/feature/auth/query/detail/user_detail_vm.dart';
import 'package:flutter_challenge_george/routes/app_pages.dart';
import 'package:flutter_challenge_george/shared/common/common_widget.dart';
import 'package:flutter_challenge_george/shared/constants/constant_storage.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeController extends GetxController {
  final UserDetailHandler userDetailHandler;

  HomeController({required this.userDetailHandler});
  var pref = Get.find<SharedPreferences>();

  var userDetailVM = Rxn<UserDetailVM>();
  @override
  void onInit() {
    super.onInit();
    getUserDetail();
  }


  Future<void> getUserDetail() async {
    var userId = pref.getString(ConstantStorage.userId);
    UserDetailQuery request = UserDetailQuery(userId: userId ?? "");
    var res = await userDetailHandler.handleAsync(request);
    if (res != null) {
      if (res.success) {
        userDetailVM.value = res.data;
        userDetailVM.refresh();
      }else{
        await CommonWidget.showInfo(
          res.message,
          isSuccess: res.success,
        );
      }
    }
  }

  void logOut() {
    pref.remove(ConstantStorage.userId);
    goToInitial();
  }
}
