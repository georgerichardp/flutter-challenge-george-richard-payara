import 'package:flutter/material.dart';
import 'package:flutter_challenge_george/modules/home/home_controller.dart';
import 'package:flutter_challenge_george/shared/common/common_widget.dart';
import 'package:flutter_challenge_george/shared/widget/text_tile_widget.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset("assets/images/header-splash.png"),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      Image.asset("assets/icons/logo.png"),
                      CommonWidget.rowHeight(32),
                      Text(
                        "welcome".tr,
                        style: context.textTheme.headline4,
                      ),
                    ],
                  ),
                ),
                CommonWidget.rowHeight(40),
                Obx(() {
                  if (controller.userDetailVM.value != null) {
                    var item = controller.userDetailVM.value!;
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextTileWidget(
                          title: "user_id".tr,
                          subTitle: item.userId,
                        ),
                        TextTileWidget(
                          title: "user_name".tr,
                          subTitle: item.userName,
                        ),
                        TextTileWidget(
                          title: "phone_number".tr,
                          subTitle: item.phoneNumber,
                        ),
                      ],
                    );
                  }
                  return const Center(child: CircularProgressIndicator());
                }),
                CommonWidget.rowHeight(),
                Align(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    onPressed: () {
                      CommonWidget.showAlert(
                        "logout_alert".tr,
                        pressedOk: () {
                          controller.logOut();
                        },
                      );
                    },
                    child: Text("log_out".tr),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
