import 'package:flutter_challenge_george/core/application/feature/auth/query/detail/user_detail_handler.dart';
import 'package:flutter_challenge_george/modules/home/home_controller.dart';
import 'package:get/get.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => HomeController(
        userDetailHandler: UserDetailHandler(authRepository: Get.find()),
      ),
    );
  }
}
