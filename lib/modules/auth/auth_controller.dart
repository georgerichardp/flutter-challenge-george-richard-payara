import 'package:flutter/material.dart';
import 'package:flutter_challenge_george/core/application/feature/auth/command/login/login_command.dart';
import 'package:flutter_challenge_george/core/application/feature/auth/command/login/login_handler.dart';
import 'package:flutter_challenge_george/core/application/feature/auth/command/register/register_command.dart';
import 'package:flutter_challenge_george/core/application/feature/auth/command/register/register_handler.dart';
import 'package:flutter_challenge_george/routes/app_pages.dart';
import 'package:flutter_challenge_george/shared/common/common_string.dart';
import 'package:flutter_challenge_george/shared/common/common_widget.dart';
import 'package:flutter_challenge_george/shared/constants/constant_storage.dart';
import 'package:flutter_challenge_george/shared/failure/request_error.dart';
import 'package:flutter_challenge_george/shared/failure/response_error.dart';
import 'package:flutter_challenge_george/shared/model/simple_model.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

class AuthController extends GetxController {
  final LoginHandler loginHandler;
  final RegisterHandler registerHandler;

  AuthController({
    required this.loginHandler,
    required this.registerHandler,
  });

  GlobalKey keyLanguage = GlobalKey();
  GlobalKey<FormState> keyFormLogin = GlobalKey();
  GlobalKey<FormState> keyFormRegister = GlobalKey();
  late TutorialCoachMark tutorialCoachMark;
  List<TargetFocus> targets = [];
  List<SimpleModel<String>> listLanguage = [
    SimpleModel(
      title: "English",
      data: "assets/icons/united-states.png",
    ),
    SimpleModel(
      title: "Indonesia",
      data: "assets/icons/indonesia.png",
    )
  ];
  var valueLanguage = Rxn<SimpleModel>();
  var pref = Get.find<SharedPreferences>();
  //Text editing controller login
  TextEditingController controllerUser = TextEditingController();
  TextEditingController controllerPass = TextEditingController();
  //Text editing controller register
  TextEditingController controllerRegName = TextEditingController();
  TextEditingController controllerRegPass = TextEditingController();
  TextEditingController controllerRegUserId = TextEditingController();
  TextEditingController controllerRegConfirmPass = TextEditingController();
  TextEditingController controllerRegPhone= TextEditingController();
  var obscurePass = Rxn<bool>(true);
  var obscurePassReq = Rxn<bool>(true);
  @override
  void onInit() {
    super.onInit();
    setLanguage();
  }

  bool initialLogin() {
    return pref.getBool(ConstantStorage.initialLogin) ?? false;
  }

  void showTutorial(BuildContext context) {
    initTargets();
    tutorialCoachMark = TutorialCoachMark(
      context,
      targets: targets,
      colorShadow: Colors.black,
      textSkip: "SKIP",
      paddingFocus: 10,
      opacityShadow: 0.6,
      alignSkip: Alignment.bottomRight,
      onFinish: () {
        pref.setBool(ConstantStorage.initialLogin, true);
      },
      onSkip: () {
        pref.setBool(ConstantStorage.initialLogin, true);
      },
    )..show();
  }

  void initTargets() {
    targets.clear();
    targets.add(
      TargetFocus(identify: "Target 1", keyTarget: keyLanguage, contents: [
        TargetContent(
            align: ContentAlign.bottom,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "language".tr,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 20.0),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Text(
                    "language_set_info".tr,
                    style: const TextStyle(color: Colors.white),
                  ),
                )
              ],
            ))
      ]),
    );
  }

  void updateLanguage(String language) {
    Locale locale = const Locale("id", "ID");
    if (language == "English") {
      locale = const Locale("en", "US");
    }
    pref.setString(ConstantStorage.language, locale.languageCode);
    Get.updateLocale(locale);
    setLanguage();
  }

  void setLanguage() {
    if (pref.getString(ConstantStorage.language) == "id") {
      valueLanguage(listLanguage[1]);
    } else {
      valueLanguage(listLanguage[0]);
    }
  }

  Future<void> submitLogin() async {
    keyFormLogin.currentState!.validate();
    LoginCommand command = LoginCommand(
      user: controllerUser.text,
      password: controllerPass.text,
    );
    try {
      var res = await loginHandler.handleAsync(command);
      if (res != null) {
        await CommonWidget.showInfo(CommonString.translate(res.message),isSuccess: res.success);
        if (res.success) {
          pref.setString(ConstantStorage.userId, res.data!.userId);
          goToInitial();
        }
      }
    } on ResponseError catch (e) {
      CommonWidget.showInfo(CommonString.translate(e.message));
    } on RequestError catch (e) {
      CommonWidget.showInfo(CommonString.translate(e.message));
    }
  }

  Future<void> submitRegister() async {
    keyFormRegister.currentState!.validate();
    RegisterCommand command = RegisterCommand(
      userName: controllerRegName.text,
      userId: controllerRegUserId.text,
      password: controllerRegPass.text,
      phoneNumber: controllerRegPhone.text,
      confirmPassword: controllerRegConfirmPass.text
    );
    try {
      var res = await registerHandler.handleAsync(command);
      if (res != null) {
        await CommonWidget.showInfo(CommonString.translate(res.message),isSuccess: res.success);
        if (res.success) {
          disposeRegister();
        }
      }
    } on ResponseError catch (e) {
      CommonWidget.showInfo(CommonString.translate(e.message));
    } on RequestError catch (e) {
      CommonWidget.showInfo(CommonString.translate(e.message));
    }
  }
  void disposeRegister(){
    Get.back();
    controllerRegName.clear();
    controllerRegUserId.clear();
    controllerRegPass.clear();
    controllerRegConfirmPass.clear();
    controllerRegPhone.clear();
  }
}
