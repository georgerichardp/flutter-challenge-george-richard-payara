import 'package:flutter/material.dart';
import 'package:flutter_challenge_george/modules/auth/auth_controller.dart';
import 'package:flutter_challenge_george/shared/common/common_widget.dart';
import 'package:flutter_challenge_george/shared/widget/text_field_widget.dart';
import 'package:get/get.dart';

class RegisterPage extends StatelessWidget {
  final AuthController controller = Get.arguments;
  RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("sign_up".tr),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Obx(() {
              return Form(
                key: controller.keyFormRegister,
                child: Column(
                  children: [
                    Image.asset("assets/icons/logo.png"),
                    TextFieldWidget(
                      labelText: "user_id".tr,
                      hintText: "user_id".tr,
                      controller: controller.controllerRegUserId,
                      textInputAction: TextInputAction.next,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "insert".tr+" "+"user_id".tr;
                        }
                      },
                    ),
                    TextFieldWidget(
                      labelText: "name".tr,
                      hintText: "name".tr,
                      controller: controller.controllerRegName,
                      textInputAction: TextInputAction.next,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "insert".tr+" "+"name".tr;
                        }
                      },
                    ),
                    TextFieldWidget(
                      labelText: "phone_number".tr,
                      hintText: "phone_number".tr,
                      controller: controller.controllerRegPhone,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.phone,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "insert".tr+" "+"phone_number".tr.toLowerCase();
                        }
                      },
                    ),
                    TextFieldWidget(
                      labelText: "password".tr,
                      hintText: "password".tr,
                      controller: controller.controllerRegPass,
                      obscureText: controller.obscurePassReq.value!,
                      textInputAction: TextInputAction.next,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "insert".tr+" "+"password".tr.toLowerCase();
                        }
                      },
                    ),
                    TextFieldWidget(
                      labelText: "confirm".tr + " " + "password".tr,
                      hintText: "confirm".tr + " " + "password".tr,
                      controller: controller.controllerRegConfirmPass,
                      obscureText: controller.obscurePassReq.value!,
                      textInputAction: TextInputAction.done,
                      suffixIcon: InkWell(
                        onTap: () {
                          controller.obscurePassReq(
                              !controller.obscurePassReq.value!);
                        },
                        child: Icon(controller.obscurePassReq.value!
                            ? Icons.visibility_off
                            : Icons.visibility),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "insert".tr+" "+"confirm".tr+" "+"password".tr.toLowerCase();
                        }
                        if (value != controller.controllerRegPass.text) {
                          return "pass_not_match".tr;
                        }
                      },
                    ),
                    CommonWidget.rowHeight(),
                    CommonWidget.rowHeight(),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: ElevatedButton(
                        onPressed: () {
                          controller.submitRegister();
                        },
                        child: Text("sign_up".tr.toUpperCase()),
                      ),
                    )
                  ],
                ),
              );
            }
          ),
        ),
      ),
    );
  }
}
