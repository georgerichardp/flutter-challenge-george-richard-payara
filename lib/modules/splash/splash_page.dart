import 'package:flutter/material.dart';
import 'package:flutter_challenge_george/modules/home/home_controller.dart';
import 'package:get/get.dart';
class SplashPage extends GetView<HomeController>{
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset("assets/images/header-splash.png"),
          Expanded(child: Image.asset("assets/icons/logo.png")),
          Image.asset("assets/images/footer-splash.png"),
        ],
      ),
    );
  }

}
