import 'package:flutter_challenge_george/routes/app_pages.dart';
import 'package:flutter_challenge_george/shared/constants/constant_storage.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashController extends GetxController {
  @override
  void onReady() async {
    super.onReady();
    await Future.delayed(const Duration(seconds: 2));
    var storage = Get.find<SharedPreferences>();
    try {
      if (storage.getString(ConstantStorage.userId) != null) {
        goToHome();
      } else {
        goToAuth();
      }
    } catch (e) {
      goToAuth();
    }
  }
}
