import 'package:flutter_challenge_george/core/application/repository/auth_repository.dart';
import 'package:flutter_challenge_george/infrastructure/repository/local/auth_repository_local.dart';
import 'package:get/get.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() async {
    Get.put<AuthRepository>(AuthRepositoryLocal(), permanent: true);
  }
}
