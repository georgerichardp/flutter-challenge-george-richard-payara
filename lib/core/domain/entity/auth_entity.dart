class AuthEntity {
  final String userName;
  final String userId;
  final String password;
  final String phoneNumber;

  AuthEntity({
    required this.userName,
    required this.userId,
    required this.password,
    required this.phoneNumber,
  });
}
