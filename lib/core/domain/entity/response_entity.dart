class ResponseEntity<Datum>{
  String message;
  bool success;
  Datum? data;

  ResponseEntity({required this.message, required this.success, this.data});
}