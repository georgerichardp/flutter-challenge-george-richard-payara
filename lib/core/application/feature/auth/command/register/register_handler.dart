import 'package:flutter_challenge_george/core/application/feature/auth/command/register/register_command.dart';
import 'package:flutter_challenge_george/core/application/feature/auth/command/register/register_validator.dart';
import 'package:flutter_challenge_george/core/application/feature/handler.dart';
import 'package:flutter_challenge_george/core/application/repository/auth_repository.dart';
import 'package:flutter_challenge_george/core/domain/entity/response_entity.dart';

class RegisterHandler extends Handler<ResponseEntity, RegisterCommand> {
  final AuthRepository authRepository;

  RegisterHandler({required this.authRepository});

  @override
  Future<ResponseEntity?> handleAsync(RegisterCommand request) async {
    RegisterValidator().validate(request);
    return authRepository.register(request);
  }
}
