import 'package:flutter_challenge_george/core/application/feature/auth/command/register/register_command.dart';
import 'package:flutter_challenge_george/core/application/feature/command_validator.dart';
import 'package:flutter_challenge_george/shared/failure/request_error.dart';

class RegisterValidator extends CommandValidator<RegisterCommand>{
  @override
  void validate(RegisterCommand element) {
    if (element.userId.isEmpty) {
      throw RequestError(message: "please_insert#user_id");
    }
    if (element.userName.isEmpty) {
      throw RequestError(message: "please_insert#name");
    }
    if (element.phoneNumber.isEmpty) {
      throw RequestError(message: "please_insert#phone_number");
    }
    if (element.password.isEmpty) {
      throw RequestError(message: "please_insert#password");
    }
    if (element.confirmPassword.isEmpty) {
      throw RequestError(message: "please_insert#confirm#password");
    }
    if (element.password != element.confirmPassword) {
      throw RequestError(message: "pass_not_match");
    }
  }

}