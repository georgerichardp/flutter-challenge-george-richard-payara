class RegisterCommand {
  final String userName;
  final String userId;
  final String password;
  final String confirmPassword;
  final String phoneNumber;

  RegisterCommand(
      {required this.userName,
      required this.userId,
      required this.confirmPassword,
      required this.password,
      required this.phoneNumber});

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "user_name": userName,
    "phone_number": phoneNumber,
    "password": confirmPassword,
  };

}
