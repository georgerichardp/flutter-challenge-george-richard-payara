import 'package:flutter_challenge_george/core/application/feature/auth/command/login/login_command.dart';
import 'package:flutter_challenge_george/core/application/feature/command_validator.dart';
import 'package:flutter_challenge_george/shared/failure/request_error.dart';

class LoginValidator extends CommandValidator<LoginCommand>{
  @override
  void validate(LoginCommand element) {
    if (element.user.isEmpty) {
      throw RequestError(message: "please_insert#user_id");
    }
    if (element.password.isEmpty) {
      throw RequestError(message: "please_insert#password");
    }
  }

}