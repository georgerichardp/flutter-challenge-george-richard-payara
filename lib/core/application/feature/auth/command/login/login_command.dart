class LoginCommand{
  final String user;
  final String password;

  LoginCommand({required this.user, required this.password});
}