class UserDetailQuery {
  final String userId;

  UserDetailQuery({
    required this.userId,
  });
}
