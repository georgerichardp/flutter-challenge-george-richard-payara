import 'package:flutter_challenge_george/core/application/feature/auth/query/detail/user_detail_query.dart';
import 'package:flutter_challenge_george/core/application/feature/auth/query/detail/user_detail_vm.dart';
import 'package:flutter_challenge_george/core/application/feature/handler.dart';
import 'package:flutter_challenge_george/core/application/repository/auth_repository.dart';
import 'package:flutter_challenge_george/core/domain/entity/response_entity.dart';

class UserDetailHandler
    extends Handler<ResponseEntity<UserDetailVM>, UserDetailQuery> {
  AuthRepository authRepository;

  UserDetailHandler({required this.authRepository});

  @override
  Future<ResponseEntity<UserDetailVM>?> handleAsync(
      UserDetailQuery request) async {
    var res = await authRepository.getDetail(request);
    if (res != null) {
      return ResponseEntity(
        message: res.message,
        success: res.success,
        data: res.data==null?null:UserDetailVM(
          userName: res.data!.password,
          userId: res.data!.userId,
          password: res.data!.password,
          phoneNumber: res.data!.phoneNumber,
        ),
      );
    }
  }
}
