abstract class Handler<E,K> {
  Future<E?> handleAsync(K request);
}