import 'dart:convert';

import 'package:flutter_challenge_george/core/application/feature/auth/command/login/login_command.dart';
import 'package:flutter_challenge_george/core/application/feature/auth/command/register/register_command.dart';
import 'package:flutter_challenge_george/core/application/feature/auth/query/detail/user_detail_query.dart';
import 'package:flutter_challenge_george/core/application/repository/auth_repository.dart';
import 'package:flutter_challenge_george/core/domain/entity/auth_entity.dart';
import 'package:flutter_challenge_george/core/domain/entity/response_entity.dart';
import 'package:flutter_challenge_george/infrastructure/model/auth_model.dart';
import 'package:flutter_challenge_george/shared/common/common_widget.dart';
import 'package:flutter_challenge_george/shared/constants/constant_storage.dart';
import 'package:flutter_challenge_george/shared/failure/response_error.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthRepositoryLocal extends AuthRepository {
  var pref = Get.find<SharedPreferences>();
  @override
  Future<ResponseEntity<AuthEntity>?> getDetail(UserDetailQuery query) async {
    await Future.delayed(const Duration(seconds: 2));
    try {
      var res = pref.getStringList(ConstantStorage.listAuth) ?? [];
      var data = res.map((e) => AuthModel.fromJson(jsonDecode(e))).toList();
      AuthEntity? entity;
      entity = data.singleWhere((element) => element.userId == query.userId);
      return ResponseEntity(message: "Success", success: true, data: entity);
    } catch (_) {
      throw ResponseError(message: "User data not found");
    }
  }

  @override
  Future<ResponseEntity<AuthEntity>?> login(LoginCommand command) async {
    CommonWidget.showLoading();
    await Future.delayed(const Duration(seconds: 2));
    var res = pref.getStringList(ConstantStorage.listAuth) ?? [];
    var data = res.map((e) => AuthModel.fromJson(jsonDecode(e))).toList();
    AuthEntity? entity;
    for (var i in data) {
      if (i.userId==command.user) {
        entity = i;
      }
    }
    if (entity != null) {
      if (entity.userId == command.user &&
          entity.password == command.password) {
        CommonWidget.dismissLoading();
        return ResponseEntity(
          message: "login_success",
          success: true,
          data: entity,
        );
      } else {
        CommonWidget.dismissLoading();
        return ResponseEntity(
          message: "password_wrong",
          success: false,
        );
      }
    }else{
      CommonWidget.dismissLoading();
      throw ResponseError(message: "user_not_register#please#sign_up");
    }
  }

  @override
  Future<ResponseEntity?> register(RegisterCommand command) async {
    CommonWidget.showLoading();
    await Future.delayed(const Duration(seconds: 2));
    var res = pref.getStringList(ConstantStorage.listAuth) ?? [];
    var data = res.map((e) => AuthModel.fromJson(jsonDecode(e))).toList();
    AuthEntity? entity;
    for (var i in data) {
      if (i.userId==command.userId) {
        entity = i;
      }
    }
    if (entity == null) {
      AuthModel authModel = AuthModel.fromJson(command.toJson());
      res.add(jsonEncode(authModel.toJson()));
      pref.setStringList(ConstantStorage.listAuth, res);
      CommonWidget.dismissLoading();
      return ResponseEntity(
        message: "register_success",
        success: true,
      );
    }else{
      CommonWidget.dismissLoading();
      return ResponseEntity(
        message: "user_already",
        success: false,
      );
    }
  }
}
