import 'package:flutter_challenge_george/core/domain/entity/auth_entity.dart';

class AuthModel extends AuthEntity{
  AuthModel.fromJson(Map<String,dynamic> json) : super(
    userId: json["user_id"],
    userName: json["user_name"],
    phoneNumber: json["phone_number"],
    password: json["password"],
  );
  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "user_name": userName,
    "phone_number": phoneNumber,
    "password": password,
  };
  static List<AuthEntity> parseEntries(List<dynamic> entries){
    return entries.map((e) => AuthModel.fromJson(e)).toList();
  }

}